This project contains my SolidWorks certificates in PDF

<br>

| Subject                                                       | Issued On                 | Certificate ID        |
| --------------------------------------------------------------|---------------------------|-----------------------|
| [CSWA](/Certificate_C-8LPHSULABB.pdf)                         | 2014-02-27                | C-8LPHSULABB          |
| [Weldments](/Certificate_C-KSH4A9E8FP.pdf)                    | 2014-02-28                | C-KSH4A9E8FP          |
| [CSWP](/Certificate_C-U52H5Z9J8A.pdf)                         | 2014-03-17                | C-U52H5Z9J8A          |
| [Sheet Metal](/Certificate_C-3MV8T4UPFY.pdf)                  | 2014-04-28                | C-3MV8T4UPFY          |
| [Drawing Tools](/Certificate_C-2ZLZTFFTSV.pdf)                | 2014-12-08                | C-2ZLZTFFTSV          |
| [Mold Making](/Certificate_C-T8GZBJW6KN.pdf)                  | 2015-05-17                | C-T8GZBJW6KN          |
| [CSWE](/Certificate_C-6R8UEEJ4X7.pdf)                         | 2015-12-01                | C-6R8UEEJ4X7          |
| [DriveWorks Xpress](/Certificate-DWX2017171278399.pdf)        | 2017-12-17                | DWX2017171278399      |
| [Surfacing](/CSWPA-SU_C-V2FVG9VQRZ.pdf)                       | 2018-02-17                | C-V2FVG9VQRZ          |
| [Additive Manufacturing](/Certificate_C-LKATQRCNLK.pdf)       | 2018-11-05                | C-LKATQRCNLK          |
| [SW Power User CAD](/SWPUC_20_-_Fatih_Mehmet_Ozcan_-_CAD.pdf) | 2018-12-31                | --                    |
| [SW Power User Programmer](/SWPUC_20_-_Fatih_Mehmet_Ozcan.pdf)| 2018-12-31                | --                    |
| [API](/Certificate_C-STKE9ZFPQ5.pdf)                          | 2020-12-05                | C-STKE9ZFPQ5          |
| [SolidWorks Champion](https://www.credly.com/badges/299bb037-063a-4071-bd22-4425362f5d5e/public_url)                                       | 2021-07-16                | --                    |

<br>
For more information about certification

http://www.solidworks.com/sw/support/solidworks-certification.htm

<br>
To validate certificates and see how many people have which certificate in any country

https://solidworks.virtualtester.com/#userdir_button